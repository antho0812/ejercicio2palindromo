/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palindromo;

import java.util.Scanner;

/**
 *
 * @author Anthony Mauricio
 */
public class Palindromos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Palindromos p1 = new Palindromos();
        System.out.println ("VALIDADOR DE PALABRAS PALINDROMOS");
        System.out.println ("Introduzca una palabra:");
        String et = "";
        Scanner entradaEscaner = new Scanner (System.in);
        et = entradaEscaner.nextLine ();
		if(et.equals("")){
			System.out.println ("La palabra no puede ser vacia");
			return;
		}
        System.out.println("La Palabra Ingresada  "+p1.palindroma(et));

    }

    public String palindroma(String palabra){
        String mensaje="";
        int inc = 0;
        int des = palabra.length()-1;
        boolean bError = false;
        
        while ((inc<des) && (!bError)){
			
	if (palabra.charAt(inc)==palabra.charAt(des)){				
		inc++;
		des--;
	} else {
		bError = true;
	}
       }
        if (!bError)
			mensaje="ES UN PALINDROMO";
        else
			mensaje="NO ES UN PALINDROMO";
        
        return mensaje;
    
    }
   

}
